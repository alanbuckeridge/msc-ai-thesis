from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Thesis submitted for MSc in Artificial Intelligence, University of Limerick',
    author='Alan Buckeridge',
    license='MIT',
)
