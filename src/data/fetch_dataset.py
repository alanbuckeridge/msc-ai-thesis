# -*- coding: utf-8 -*-
import click
import json
import logging
import re
import requests
import wikipedia
import pandas as pd
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
from SPARQLWrapper import SPARQLWrapper, JSON
from tqdm import tqdm
from urllib.parse import quote, unquote
from bs4 import BeautifulSoup

@click.command()
@click.argument('output_filepath', type=click.Path())
def main(output_filepath):
    """ Retrieves raw data from Wikipedia and saves it in (../raw).
    """
    logger = logging.getLogger(__name__)
    logger.info('Retrieving raw data from Wikipedia and storing it in '
                + output_filepath)

    df = get_wikidata_diseases()

    df['Summary'] = None
    df['Redirected_To'] = None
    df['Cochrane_Reviews'] = None
    url_prefix_length = len("https://en.wikipedia.org/wiki/")

    for index, row in tqdm(df.iterrows(), desc='Progress', total=len(df), unit="WkD_disease"):
        if row['WP_en_article'] is not None:
            raw_wp_title = row['WP_en_article'][url_prefix_length:]
            wp_title = unquote(unquote(raw_wp_title))
            wikipedia_page = wikipedia.WikipediaPage(title=wp_title)
            row['Summary'] = wikipedia_page.summary
            if wp_title.casefold() != wikipedia_page.title.casefold().replace(" ", "_"):
                row['Redirected_To'] = wikipedia_page.url

            # Fetch the raw HTML
            r = requests.get(wikipedia_page.url)
            wikipedia_html = r.text

            # Get any Cochrane Review citations
            citations = []
            soup = BeautifulSoup(wikipedia_html, 'html.parser')
            for elem in soup.select("cite.journal"):
                if elem.find(text=re.compile(r'Cochrane Database')):
                    links = elem.select('a[href="/wiki/Digital_object_identifier"]')
                    for link in links:
                        doi = link.find_next_sibling('a')
                        citations.append(doi.text)
            if citations:
                row['Cochrane_Reviews'] = str.join("|", citations)

            # Write a copy of the HTML to the RAW_DATA directory
            html_file = output_filepath + '/' + quote(raw_wp_title, safe='') + '.html'
            with open(html_file, 'w') as f:
                f.write(wikipedia_html)

    csv_file = output_filepath + '/wikipedia_data.csv'
    df.to_csv(csv_file)


def get_wikidata_diseases():
    """
    WikiData search: searches WkD for a list of diseases

    This is a modified version of code from: 
    1. https://lawlesst.github.io/notebook/sparql-dataframe.html
    2. https://github.com/SuLab/sparql_to_pandas/blob/master/SPARQL_pandas.ipynb
    """

    wds = "https://query.wikidata.org/sparql"

    rq = """
    SELECT ?WkD_disease ?WkD_diseaseLabel ?WP_en_article 
    WHERE {
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        ?WkD_disease wdt:P31 wd:Q12136.

        OPTIONAL {
            ?WP_en_article schema:about ?WkD_disease .
            ?WP_en_article schema:inLanguage "en" .
            ?WP_en_article schema:isPartOf <https://en.wikipedia.org/> .
        }
    }
    #order by desc(?WkD_disease)
    """

    return get_sparql_dataframe(wds, rq)


def get_sparql_dataframe(service, query):
    """
    Helper function to convert SPARQL results into a Pandas data frame.
    """
    sparql = SPARQLWrapper(service)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    result = sparql.query()

    processed_results = json.load(result.response)
    cols = processed_results['head']['vars']

    out = []
    for row in processed_results['results']['bindings']:
        item = []
        for c in cols:
            item.append(row.get(c, {}).get('value'))
        out.append(item)

    return pd.DataFrame(out, columns=cols)



if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
